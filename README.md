Projekt se nachází ve vývojové části. Poslední úpravy naleznete ve větvi develo. Stabilní verze je ve větvi master.

Pro zprovoznění projektu si vytvořte clone develop větve (případně můžete stáhnout jaho zip, který je následně potřeba rozbalit).

Prerekvizity:

1) Node.js - https://nodejs.org/en/  - stáhnout a nainstalovat
2) Angular 9 - https://angular.io/guide/setup-local - přes příkazovou řádku spustit - npm install -g @angular/cli
3) Ionic 5 - https://ionicframework.com/docs/intro/cli - přes příkazovou řádku spusit - npm install -g @ionic/cli
4) Ve složce se stáhnutým projektem přes příkazovou řádku spusťte - ionic serve

Projekt se defaultně nastartuje na adrese "http://localhost:8100/"

Doporučené vývojové prostředí:

VisualCodeStudio - oficiálně doporučené prostředí, které poskytuje všechnu potřebnou podporu vývoje


// poznamka pro Market, kdyby si to zkoušela a nebylo to takhle jednoduché, tak prosím dej vědět, kde ses sekla 
(možná bude potřeba v rámci projektu spustit ještě "npm install" nebo tak něco a nechci odinstalovávat všechno jen abych to zjistil