import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ZooPage} from './zoo.page';


const routes: Routes = [
    {
        path: 'tabs',
        component: ZooPage,
        children: [
            {
                path: 'home',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
                    }
                ]
            },
            {
                path: 'map',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./map/map.module').then(m => m.MapPageModule)
                    }
                ]
            },
            {
                path: 'lexicon',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./lexicon/lexicon.module').then(m => m.LexiconPageModule)
                    },
                    {
                        path: ':animalId',
                        loadChildren: () => import('../animal-detail/animal-detail.module').then(m => m.AnimalDetailPageModule)
                    }
                ]
            },
            {
                path: 'services',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./services/services.module').then(m => m.ServicesPageModule)
                    },
                ]
            },
            {
                path: 'calendar',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('./calendar/calendar.module').then(m => m.CalendarPageModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/zoo/tabs/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/zoo/tabs/home',
        pathMatch: 'full'
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ZooRoutingModule {
}
