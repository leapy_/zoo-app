import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';

import {ZooPage} from './zoo.page';
import {ZooRoutingModule} from './zoo-routing.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ZooRoutingModule
    ],
    declarations: [ZooPage]
})
export class ZooPageModule {
}
