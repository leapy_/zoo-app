import {Injectable} from '@angular/core';
import {Animal} from './models/animal.model';
import {HttpClient} from '@angular/common/http';
import {map, take, tap} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';

interface AnimalData {
    description: string;
    imageUrl: string;
    name: string;
}

@Injectable({
    providedIn: 'root'
})
export class AnimalsService {

    private animals = new BehaviorSubject<Animal[]>([]);


    constructor(private httpService: HttpClient) {
    }

    get getAnimals() {
        return this.animals.asObservable();
    }

    fetchAnimal() {
        return this.httpService
            .get<{ [key: string]: AnimalData }>('https://zoo-app-adab7.firebaseio.com/animals.json')
            .pipe(map(resData => {
                    const animal = [];
                    for (const key in resData) {
                        if (resData.hasOwnProperty(key)) {
                            animal
                                .push(new Animal(
                                    key
                                    , resData[key].name
                                    , resData[key].description
                                    , resData[key].imageUrl));
                        }
                    }
                    return animal;
                }),
                tap(places => {
                    this.animals.next(places);
                })
            );
    }

    uploadeMockToFirebase() {
        return this.httpService.post('https://zoo-app-adab7.firebaseio.com/animals.json', {...this.animals[0], id: null})
            .pipe(tap(respData => {
                console.log(respData);
            }));
    }

    getAnimal(id: string) {
        return this.animals.pipe(
            take(1),
            map(animal => {
                return {...animal.find(p => p.id === id)};
            })
        );
    }

}
