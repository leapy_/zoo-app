import {Component, OnInit} from '@angular/core';
import {AnimalsService} from '../animals.service';
import {Animal} from '../models/animal.model';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-lexicon',
    templateUrl: './lexicon.page.html',
    styleUrls: ['./lexicon.page.scss'],
})
export class LexiconPage implements OnInit {

    animals: Animal[];
    private animalSub: Subscription;

    constructor(private animalsService: AnimalsService) {
    }

    ngOnInit() {
        this.animalSub = this.animalsService.getAnimals.subscribe(places => {
            this.animals = places;
        });
    }

    ionViewWillEnter() {
        this.animalsService.fetchAnimal().subscribe();
        // this.animalsService.uploadeMockToFirebase().subscribe();
    }

}
