import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IonicModule} from '@ionic/angular';

import {LexiconPage} from './lexicon.page';

describe('LexiconPage', () => {
    let component: LexiconPage;
    let fixture: ComponentFixture<LexiconPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LexiconPage],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(LexiconPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
