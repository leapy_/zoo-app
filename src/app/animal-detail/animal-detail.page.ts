import {Component, OnInit} from '@angular/core';
import {Animal} from '../zoo/models/animal.model';
import {AnimalsService} from '../zoo/animals.service';
import {ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-animal-detail',
    templateUrl: './animal-detail.page.html',
    styleUrls: ['./animal-detail.page.scss'],
})
export class AnimalDetailPage implements OnInit {

    animal: Animal;
    private animalSub: Subscription;

    constructor(private route: ActivatedRoute, private navCtrl: NavController, private animalsService: AnimalsService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe(paramMap => {
            if (!paramMap.has('animalId')) {
                this.navCtrl.navigateBack('/places/tabs/discover');
                return;
            }
            this.animalSub = this.animalsService
                .getAnimal(paramMap.get('animalId'))
                .subscribe(animal => {
                    this.animal = animal;
                });
        });
    }

}
